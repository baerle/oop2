package de.iisys.va.oop2.javafx.propsplus;

import javafx.beans.binding.Bindings;
import javafx.beans.binding.LongBinding;
import javafx.scene.control.TextField;
import javafx.scene.layout.VBox;

public class DetailView extends VBox {

    private final TextField name;
    private final TextField matNr;
    private final LongBinding matAsLong;

    private Student data;

    public DetailView() {
        name = new TextField();
        matNr = new TextField();
        matAsLong = Bindings.createLongBinding(() -> Long.parseLong(matNr.textProperty().get()), matNr.textProperty());
        this.setSpacing(2.0);
        this.getChildren().addAll(name,matNr);
    }

    public void block(){
        unbindStudent();
        name.setEditable(false);
        matNr.setEditable(false);
    }

    public void setStudent(Student s){
        if(s == data) return;
        unbindStudent();
        data = s;
        name.setText(data.getName());
        matNr.setText(""+data.getMatNr());
        data.nameProperty().bind(name.textProperty());
        data.matNrProperty().bind(matAsLong);
    }

    private void unbindStudent(){
        if(data != null){
            data.nameProperty().unbind();
            data.matNrProperty().unbind();
        }
    }
}
