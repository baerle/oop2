package de.iisys.va.oop2.javafx.propsplus;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class App extends Application {
    @Override
    public void start(Stage primaryStage) throws Exception {
        MainView mv = new MainView();
        Scene s = new Scene(mv, 500,200);
        primaryStage.setScene(s);
        primaryStage.show();
    }
}
