package de.iisys.va.oop2.javafx.propsplus;

import javafx.beans.Observable;
import javafx.beans.property.ListProperty;
import javafx.beans.property.SimpleListProperty;
import javafx.collections.FXCollections;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

public class MainView extends HBox {

    private final ListView<Student> list;
    private final DetailView details;

    public MainView() {
        ListProperty<Student> studentListProperty = new SimpleListProperty<>(
                FXCollections.observableArrayList(
                (Student student) -> new Observable[]{student.nameProperty(), student.matNrProperty()}
        ));

        studentListProperty.add(new Student("Hans Maier", 123123));
        studentListProperty.add(new Student("Hannelore Schmid", 232323));

        list = new ListView<>();
        details = new DetailView();

        list.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            //System.out.println("adasdasd");
            if(newValue != null) details.setStudent(newValue);
        });

        list.itemsProperty().bind(studentListProperty);
        details.setStudent(studentListProperty.get(0));

        VBox controls = new VBox(2.0);
        Button sort = new Button("Sort");
        Button add = new Button("+");
        sort.setOnAction(event -> FXCollections.sort(studentListProperty.get()));
        add.setOnAction(event -> {
            studentListProperty.add(0, new Student());
            list.getSelectionModel().select(0);
        });
        controls.getChildren().addAll(sort,add);

        this.setSpacing(3.0);
        this.getChildren().addAll(controls, list, details);
    }
}
