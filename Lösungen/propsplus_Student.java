package de.iisys.va.oop2.javafx.propsplus;

import javafx.beans.property.LongProperty;
import javafx.beans.property.SimpleLongProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class Student implements Comparable {

    private StringProperty name;
    private LongProperty matNr;

    public Student(){
        name = new SimpleStringProperty("");
        matNr = new SimpleLongProperty(0);
    }

    public Student(String name, long matNr){
        this();
        this.name.set(name);
        this.matNr.set(matNr);
    }

    public String getName() {
        return name.get();
    }

    public StringProperty nameProperty() {
        return name;
    }

    public void setName(String name) {
        this.name.set(name);
    }

    public long getMatNr() {
        return matNr.get();
    }

    public LongProperty matNrProperty() {
        return matNr;
    }

    public void setMatNr(long matNr) {
        this.matNr.set(matNr);
    }

    @Override
    public String toString() {
        return "Name: "+getName()+"\nMatNr.: "+getMatNr();
    }

    @Override
    public int compareTo(Object o) {
        return Long.compare(this.matNr.get(), ((Student)o).matNr.get());
    }
}
