package SubArraySum;

public class SubArraySumImproved implements SubArraySumInterface {
    private final int[] summedUp;

    public SubArraySumImproved(int[] array) {
        this.summedUp = new int[array.length];
        int sum = 0;
        for (int i = 0; i < array.length; i++) {
            sum += array[i];
            this.summedUp[i] = sum;
        }
    }

    public int getSubSum(int from, int until) {
        if (from != 0)
            return this.summedUp[until] - this.summedUp[from - 1];
        return this.summedUp[until];
    }
}
