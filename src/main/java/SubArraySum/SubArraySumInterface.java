package SubArraySum;

public interface SubArraySumInterface {
    int getSubSum(int from, int until);
}
