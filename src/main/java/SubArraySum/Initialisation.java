package SubArraySum;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Random;

public class Initialisation {

    public static void main(String[] args) {
        initializeRandomNumbers(new File("src/SubArraySum/randoms.txt"), 10000, 100);
    }

    private static void initializeRandomNumbers(File file, int count, int range) {
        Random random = new Random();
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(file))) {
            for (int i = 0; i < count; i++) {
                Integer[] randomPair = {random.nextInt() % range, random.nextInt() % range};
                if (randomPair[0] > randomPair[1]) {
                    writer.write(randomPair[1] + "," + randomPair[0]);
                } else {
                    writer.write(randomPair[0] + "," + randomPair[1]);
                }
                writer.newLine();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
