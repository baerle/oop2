package SubArraySum;

public class SubArraySum implements SubArraySumInterface {
    private final int[] array;

    public SubArraySum(int[] array) {
        this.array = array;
    }

    public int getSubSum(int from, int until) {
        int sum = 0;
        for (int i = from; i <= until; i++) {
            sum += this.array[i];
        }
        return sum;
    }
}
