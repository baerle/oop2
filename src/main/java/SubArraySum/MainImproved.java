package SubArraySum;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

public class MainImproved {
    public static void main(String[] args) {
        new MainImproved();
    }

    public MainImproved() {
        int[] array = new int[10];
        int count = 0;
        ArrayList<Integer[]> randoms = new ArrayList<>();
        try (BufferedReader reader = new BufferedReader(new FileReader("src/SubArraySum/randoms.txt"))) {
            String line;
            while ((line = reader.readLine()) != null) {
                String[] number = line.split(",");
                randoms.add(new Integer[]{Math.abs(Integer.parseInt(number[0])) % array.length, Math.abs(Integer.parseInt(number[1])) % array.length});
                if (count < array.length)
                    array[count++] = Integer.parseInt(number[0]);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }


        Stopwatch improved = new Stopwatch();
        SubArraySumImproved subArraySumImproved = new SubArraySumImproved(array);
        for (Integer[] randomArray : randoms) {
            Stopwatch stopwatch = new Stopwatch();
            printSubSum(randomArray[0], randomArray[1], subArraySumImproved.getSubSum(randomArray[0], randomArray[1]));
            System.out.println(stopwatch);
        }
        System.out.println("complete improved:" + improved);
    }

    public void printSubSum(int random1, int random2, int result) {
        System.out.println("[" + random1 + ":" + random2 + "]: " + result);
    }
}
