package Vorlesungen;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Quicksort {
    private final int[] array;

    public Quicksort(int[] array) {
        this.array = array;
        this.quicksort(0, array.length - 1);
    }

    public Quicksort(List<Integer> list) {
        this.array = new int[list.size()];
        int count = 0;
        for (int i : list) {
            this.array[count++] = i;
        }
        this.quicksort(0, array.length - 1);
    }

    /**
     * @param from int index of first element
     * @param to   int index of last element
     */
    private void quicksort(int from, int to) {
        if (from < to) {
            int pivot = partition(from, to);
            this.quicksort(from, pivot - 1);
            this.quicksort(pivot + 1, to);
        }
    }

    private int partition(int from, int to) {
        int pivot = this.array[to];
        int i = from - 1;
        for (int j = from; j < to; j++) {
            if (this.array[j] <= pivot) {
                this.exchange(++i, j);
            }
        }
        this.exchange(++i, to);
        return i;
    }

    /**
     * exchange element at position1 with position2
     *
     * @param pos  int position 1
     * @param pos2 int position 2
     */
    private void exchange(int pos, int pos2) {
        int temp = this.array[pos];
        this.array[pos] = this.array[pos2];
        this.array[pos2] = temp;
    }

    public int[] getArray() {
        return array;
    }

    public List<Integer> getList() {
        ArrayList<Integer> al = new ArrayList<>(this.array.length);
        for (int i : this.array) {
            al.add(i);
        }
        return al;
    }

    public static void main(String[] args) {
        ArrayList<Integer> randoms = new ArrayList<>();
        try (BufferedReader reader = new BufferedReader(new FileReader("src/SubArraySum/randoms.txt"))) {
            String line;
            while ((line = reader.readLine()) != null) {
                String[] number = line.split(",");
                randoms.add(Integer.parseInt(number[0]));
                randoms.add(Integer.parseInt(number[1]));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        int count = randoms.size();
//        int count = 10;
        int[] numbers = new int[count];
        for (int i = 0; i < count; i++) {
            numbers[i] = randoms.get(i);
        }
        Quicksort qs = new Quicksort(numbers);
        System.out.println(Arrays.toString(qs.getArray()));

        // Überprüfung der korrekten Sortierung
        boolean correct = true;
        int last = Integer.MIN_VALUE;
        for (int n : qs.getArray()) {
            if (last > n) {
                correct = false;
            }
            last = n;
        }
        System.out.println(correct);
    }
}
