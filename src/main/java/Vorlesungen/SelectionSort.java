package Vorlesungen;

public class SelectionSort {

    public static void main(String[] args) {
        SelectionSort test = new SelectionSort();
        int[] test1 = {10, 9, 1, 7, 3, 2, 4, 6};
        test.selectionSort(test1);

        for (int value : test1) {
            System.out.println(value);
        }
    }

    /**
     * loop-invariant: 1 - n-1
     * because the last one has to be already the highest after n-1 runs
     * best-case: O(n^2)
     * worst-case: O(n^2)
     *
     * @param array
     */
    public void selectionSort(int[] array) {
        // finding the smallest element
        for (int j = 0; j < array.length - 1; j++) {
            int smallest = j;
            for (int i = j + 1; i < array.length; i++) {
                if (array[smallest] > array[i]) {
                    smallest = i;
                }
            }

            //exchanging the smallest element
            int temp = array[j];
            array[j] = array[smallest];
            array[smallest] = temp;
        }
    }
}
