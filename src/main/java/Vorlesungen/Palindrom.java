package Vorlesungen;

import java.util.Stack;

public class Palindrom {

    public static void main(String[] args) {
        String[] test = {"ANNA", "abcd+dcba"};

        for (String string : test) {
            System.out.println("1: " + isPalindrom(string));
            System.out.println("2: " + isPalindrom2(string));
        }
    }

    public static boolean isPalindrom(String str) {
        if (str.length() == 0) return true;
        int i = 0, j = str.length() - 1;
        while (i < j) {
            if (str.charAt(i++) != str.charAt(j--))
                return false;
        }
        return true;
    }

    public static boolean isPalindrom2(String str) {
        int middle = str.length() / 2;
        Stack<Character> chars = new Stack<>();
        for (int i = 0; i < middle; i++) {
            chars.push(str.charAt(i));
        }
        if (str.length() % 2 != 0) {
            middle++;
        }
        for (int j = middle; j < str.length(); j++) {
            if (chars.pop() != str.charAt(j))
                return false;
        }
        return true;
    }
}
