package Vorlesungen;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

public class GenericQuicksort<T extends Comparable<T>> {
    private final T[] array;

    public GenericQuicksort(T[] array) {
        this.array = array;
        this.quicksort(0, array.length - 1);
    }

    /**
     * @param from int index of first element
     * @param to   int index of last element
     */
    private void quicksort(int from, int to) {
        if (from < to) {
            int pivot = partition(from, to);
            this.quicksort(from, pivot - 1);
            this.quicksort(pivot + 1, to);
        }
    }

    private int partition(int from, int to) {
        T pivot = this.array[to];
        int i = from - 1;
        for (int j = from; j < to; j++) {
            if (this.array[j].compareTo(pivot) <= 0) {
                this.exchange(++i, j);
            }
        }
        this.exchange(++i, to);
        return i;
    }

    /**
     * exchange element at position1 with position2
     *
     * @param pos  int position 1
     * @param pos2 int position 2
     */
    private void exchange(int pos, int pos2) {
        T temp = this.array[pos];
        this.array[pos] = this.array[pos2];
        this.array[pos2] = temp;
    }

    public T[] getArray() {
        return array;
    }

    public static void main(String[] args) {
        ArrayList<Integer> randoms = new ArrayList<>();
        try (BufferedReader reader = new BufferedReader(new FileReader("src/SubArraySum/randoms.txt"))) {
            String line;
            while ((line = reader.readLine()) != null) {
                String[] number = line.split(",");
                randoms.add(Integer.parseInt(number[0]));
                randoms.add(Integer.parseInt(number[1]));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        int count = randoms.size();
//        int count = 10;
        Integer[] numbers = new Integer[count];
        for (int i = 0; i < count; i++) {
            numbers[i] = randoms.get(i);
        }
        GenericQuicksort<Integer> qs = new GenericQuicksort<>(numbers);
        System.out.println(Arrays.toString(qs.getArray()));

        // Überprüfung der korrekten Sortierung
        boolean correct = true;
        int last = Integer.MIN_VALUE;
        for (int n : qs.getArray()) {
            if (last > n) {
                correct = false;
            }
            last = n;
        }
        System.out.println(correct);
    }
}
