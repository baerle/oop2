package Vorlesungen;

import java.util.Stack;

public class QueueWithTwoStacks<T> {
    Stack<T> s1, s2;

    public QueueWithTwoStacks() {
        this.s1 = new Stack<>();
        this.s2 = new Stack<>();
    }

    public void enqueue(T item) {   //push auf Stack 1
        this.s1.push(item);
    }

    public T dequeue() {    //pop auf Stack 2
        if (this.s2.size() == 0) {
            while (this.s1.size() != 0) {
                this.s2.push(this.s1.pop());
            }
        }
        return this.s2.pop();
    }

    public static void main(String[] args) {
        QueueWithTwoStacks<String> test = new QueueWithTwoStacks<>();
        test.enqueue("Nummer 1");
        test.enqueue("Nummer 2");
        test.enqueue("Nummer 3");
        System.out.println(test.dequeue());
        test.enqueue("Nummer 4");
        System.out.println(test.dequeue());
        System.out.println(test.dequeue());
        System.out.println(test.dequeue());
    }
}
