package Vorlesungen.BinaryTree;

public class Test {
    public static void main(String[] args) {
        BinaryTree<Integer> intTree = new BinaryTree<>();
        BinaryTree<Integer> intTreeCormen = new BinaryTree<>();
        intTree.insert(10);
        intTree.insert(8);
        intTree.insert(14);
        intTree.insert(16);
        intTree.insert(12);
        intTree.insert(17);
        intTree.insert(9);
        intTree.insert(7);
        intTree.insert(5);
        intTree.insert(8);
        intTree.insert(9);
        intTreeCormen.treeInsert(10);
        intTreeCormen.treeInsert(8);
        intTreeCormen.treeInsert(14);
        intTreeCormen.treeInsert(16);
        intTreeCormen.treeInsert(12);
        intTreeCormen.treeInsert(17);
        intTreeCormen.treeInsert(9);
        intTreeCormen.treeInsert(7);
        intTreeCormen.treeInsert(5);
        intTreeCormen.treeInsert(8);
        intTreeCormen.treeInsert(9);

        System.out.println(intTree);

        intTree.delete(intTree.root.left);
        System.out.println(intTree);
        System.out.println(intTree.search(9).successor().successor().data); //10
        System.out.println(intTree.search(8).successor().data); //9
        System.out.println(intTree.search(7).successor().data); //8
        System.out.println(intTree.search(5).successor().data); //7

        System.out.println("-----------------\n\n" + intTreeCormen);

        intTreeCormen.delete(intTreeCormen.root.left);
        System.out.println(intTreeCormen);
        System.out.println(intTreeCormen.search(9).successor().successor().data); //10
        System.out.println(intTreeCormen.search(8).successor().data); //9
        System.out.println(intTreeCormen.search(7).successor().data); //8
        System.out.println(intTreeCormen.search(5).successor().data); //7
    }
}
