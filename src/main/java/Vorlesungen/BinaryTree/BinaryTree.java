package Vorlesungen.BinaryTree;

public class BinaryTree<T extends Comparable<T>> {
    public Leaf<T> root;
    public static Leaf nil = new Leaf(null);

    public BinaryTree() {
        this.root = nil;
    }

    public BinaryTree(Leaf<T> root) {
        this.root = root;
    }

    public String inOrderWalk() {
        return this.root.inOrderWalk().toString();
    }

    public void insert(T data) {
        if (this.root == nil) {
            this.root = new Leaf<>(data);
        } else {
            this.root.insert(data);
        }
    }

    public void treeInsert(T data) {
        Leaf<T> y = nil, x = this.root, z = new Leaf<>(data);
        while (x != nil) {
            y = x;
            if (data.compareTo(x.data) < 0)
                x = x.left;
            else x = x.right;
        }
        z.parent = y;
        if (y == nil) this.root = z;
        else {
            if (data.compareTo(y.data) < 0)
                y.left = z;
            else y.right = z;
        }
    }

    public Leaf<T> search(T data) {
        if (this.root != nil) {
            return this.root.search(data);
        }
        return nil;
    }

    public Leaf<T> minimum() {
        if (this.root != nil) {
            return this.root.minimum();
        }
        return nil;
    }

    public Leaf<T> maximum() {
        if (this.root != nil) {
            return this.root.maximum();
        }
        return nil;
    }

    public void delete(Leaf<T> leaf) {
        if (this.root != nil) {
            Leaf<T> result = this.root.delete(leaf);
            if (result != nil)
                this.root = result;
        }
    }

    @Override
    public String toString() {
        return inOrderWalk();
    }
}