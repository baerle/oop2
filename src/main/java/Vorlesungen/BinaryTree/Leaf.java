package Vorlesungen.BinaryTree;

public class Leaf<T extends Comparable<T>> {
    public Leaf<T> parent, right, left;
    public T data;

    public Leaf(T data) {
        this.data = data;
        this.parent = BinaryTree.nil;
        this.left = BinaryTree.nil;
        this.right = BinaryTree.nil;
    }

    public Leaf(Leaf<T> parent, Leaf<T> right, Leaf<T> left, T data) {
        this.parent = parent;
        this.right = right;
        this.left = left;
        this.data = data;
    }

    protected Leaf(Leaf<T> parent, T data) {
        this.parent = parent;
        this.data = data;
        this.left = BinaryTree.nil;
        this.right = BinaryTree.nil;
    }

    public StringBuilder inOrderWalk() {
        StringBuilder part1 = new StringBuilder(), part3 = new StringBuilder();
        if (this.left != BinaryTree.nil) {
//            System.out.println(this.left.data);
            part1 = this.left.inOrderWalk();
        }
        String part2 = this.data.toString();
        if (this.right != BinaryTree.nil)
            part3 = this.right.inOrderWalk();

        return part1.append(part2).append(", ").append(part3);
    }

    public void insert(T data) {
        if (this.data.compareTo(data) < 0) {    //root < data
            if (this.right != BinaryTree.nil)
                this.right.insert(data);
            else
                this.right = new Leaf<>(this, data);
        } else {
            if (this.left != BinaryTree.nil)
                this.left.insert(data);
            else
                this.left = new Leaf<>(this, data);
        }
    }

    public Leaf<T> search(T data) {
        if (this.data != data) {
            if (this.data.compareTo(data) < 0)
                if (this.right != BinaryTree.nil)
                    return this.right.search(data);
            if (this.left != BinaryTree.nil)
                return this.left.search(data);
        }
        return this;
    }

    public Leaf<T> minimum() {
        if (this.left != BinaryTree.nil)
            return this.left.minimum();
        return this;
    }

    public Leaf<T> maximum() {
        if (this.right != BinaryTree.nil)
            return this.right.maximum();
        return this;
    }

    public Leaf<T> successor() {
        if (this.right != BinaryTree.nil)
            return this.right.minimum();
        Leaf<T> parent = this.parent, child = this;
        while (parent != BinaryTree.nil && parent.right == child) {
            child = parent;
            parent = parent.parent;
        }
        return parent;
    }

    public Leaf<T> delete(Leaf<T> leaf) {
        if (leaf.right == BinaryTree.nil)
            exchange(leaf, leaf.left);
        else if (leaf.left == BinaryTree.nil)
            exchange(leaf, leaf.right);
        else {
            Leaf<T> newRoot = leaf.right.minimum();
            if (newRoot.parent != leaf) {
                exchange(newRoot, newRoot.right);
                newRoot.right = leaf.right;
                newRoot.right.parent = newRoot;
            }
            newRoot.left = leaf.left;
            leaf.left.parent = newRoot;
            return exchange(leaf, newRoot);
        }
        return BinaryTree.nil;
    }

    private Leaf<T> exchange(Leaf<T> delete, Leaf<T> replace) {
        if (replace != BinaryTree.nil)
            replace.parent = delete.parent;
        if (delete.parent == BinaryTree.nil)
            return replace; //Root neu setzen
        else if (delete.parent.left == delete)
            delete.parent.left = replace;
        else delete.parent.right = replace;
        return BinaryTree.nil;
    }
}
