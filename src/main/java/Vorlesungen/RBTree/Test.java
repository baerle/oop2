package Vorlesungen.RBTree;

public class Test {
    public static void main(String[] args) {
        RBTree<Integer> tree = new RBTree<>();
        tree.insert(10);
        tree.insert(8);
        tree.insert(14);
        tree.insert(16);
        tree.insert(12);
        tree.insert(17);
        tree.insert(9);
        tree.insert(7);
        tree.insert(5);
        tree.insert(8);
        tree.insert(9);

        System.out.println(tree);
    }
}
