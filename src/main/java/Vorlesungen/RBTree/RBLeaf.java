package Vorlesungen.RBTree;

import Vorlesungen.BinaryTree.Leaf;

public class RBLeaf<T extends Comparable<T>> extends Leaf<T> {
    private final RBTree<T> tree;
    public RBLeaf<T> parent, right, left;
    public Color color;

    public RBLeaf(T data) {
        super(data);
        this.tree = null;
        this.color = Color.Red;
    }

    public RBLeaf(RBTree<T> tree, T data) {
        super(data);
        this.tree = tree;
        this.color = Color.Red;
    }

    public RBLeaf(RBTree<T> tree, RBLeaf<T> parent, RBLeaf<T> right, RBLeaf<T> left, T data) {
        super(parent, right, left, data);
        this.tree = tree;
        this.parent = parent;
        this.right = right;
        this.left = left;
        this.color = Color.Red;
    }

    protected RBLeaf(RBTree<T> tree, RBLeaf<T> parent, T data) {
        super(parent, data);
        this.tree = tree;
        this.parent = parent;
    }

    public void insertFixup() {
        while (this.parent.color == Color.Red) {
            if (this.parent == this.parent.parent.left) {   // left grandfather leaf
                RBLeaf<T> uncle = this.parent.parent.right;
                if (uncle.color == Color.Red) {
                    this.parent.color = Color.Black;
                    this.color = Color.Black;
                    this.parent.parent.color = Color.Red;
                    this.parent.parent.insertFixup();
                } else {
                    RBLeaf<T> z = this;
                    if (this == this.parent.right) {
                        z = this.parent;
                        this.tree.leftRotate(z);
                    }
                    z.parent.color = Color.Black;
                    z.parent.parent.color = Color.Red;
                    this.tree.rightRotate(z.parent.parent);
                }
            } else {
                RBLeaf<T> uncle = this.parent.parent.left;
                if (uncle.color == Color.Red) {
                    this.parent.color = Color.Black;
                    this.color = Color.Black;
                    this.parent.parent.color = Color.Red;
                    this.parent.parent.insertFixup();
                } else {
                    RBLeaf<T> z = this;
                    if (this == this.parent.right) {
                        z = this.parent;
                        this.tree.leftRotate(z);
                    }
                    z.parent.color = Color.Black;
                    z.parent.parent.color = Color.Red;
                    this.tree.rightRotate(z.parent.parent);
                }
            }
        }
    }

    @Override
    public StringBuilder inOrderWalk() {
        StringBuilder part1 = new StringBuilder(), part3 = new StringBuilder();
        if (this.left != RBTree.nil) {
//            System.out.println(this.left.data);
            part1 = this.left.inOrderWalk();
        }
        StringBuilder part2 = new StringBuilder(this.data.toString()).append('(').append(this.color).append(')');
        if (this.right != RBTree.nil)
            part3 = this.right.inOrderWalk();

        return part1.append(part2).append(", ").append(part3);
    }

    @Override
    public void insert(T data) {

    }

    @Override
    public Leaf<T> search(T data) {
        return super.search(data);
    }

    @Override
    public Leaf<T> minimum() {
        return super.minimum();
    }

    @Override
    public Leaf<T> maximum() {
        return super.maximum();
    }

    @Override
    public Leaf<T> successor() {
        return super.successor();
    }

    @Override
    public Leaf<T> delete(Leaf<T> leaf) {
        return super.delete(leaf);
    }
}
