package Vorlesungen.RBTree;

import Vorlesungen.BinaryTree.BinaryTree;
import Vorlesungen.BinaryTree.Leaf;

public class RBTree<T extends Comparable<T>> extends BinaryTree<T> {
    public RBLeaf<T> root;
    public static RBLeaf nil = new RBLeaf(null);

    public RBTree() {
        super();
        this.root = nil;
        nil.color = Color.Black;
    }

    public RBTree(RBLeaf<T> root) {
        super(root);
        this.root = root;
        this.root.color = Color.Black;
        nil.color = Color.Black;
    }

    public void leftRotate(RBLeaf<T> x) {
        RBLeaf<T> y = x.right;
        x.right = y.left;
        if (y.left != RBTree.nil) {
            y.left.parent = x;
        }
        y.parent = x.parent;
        if (x.parent == RBTree.nil) {
            this.root = y;
        } else if (x == x.parent.left) {
            x.parent.left = y;
        } else {
            x.parent.right = y;
        }
        y.left = x;
        x.parent = y;
    }

    public void rightRotate(RBLeaf<T> x) {
        RBLeaf<T> y = x.left;
        x.left = y.right;
        if (y.right != RBTree.nil) {
            y.right.parent = x;
        }
        y.parent = x.parent;
        if (x.parent == RBTree.nil) {
            this.root = y;
        } else if (x == x.parent.left) {
            x.parent.left = y;
        } else {
            x.parent.right = y;
        }
        y.right = x;
        x.parent = y;
    }

    @Override
    public String inOrderWalk() {
        return this.root.inOrderWalk().toString();
    }

    @Override
    public void insert(T data) {
        RBLeaf<T> z = new RBLeaf<>(this, data);
        RBLeaf<T> father = nil;
        RBLeaf<T> child = this.root;
        while (child != nil) {
            father = child;
            if (z.data.compareTo(child.data) < 0) {
                child = child.left;
            } else {
                child = child.right;
            }
        }
        z.parent = father;
        if (father == nil) {
            this.root = z;
        } else {
            if (z.data.compareTo(father.data) < 0) {
                father.left = z;
            } else {
                father.right = z;
            }
        }
        z.left = nil;
        z.right = nil;
        z.insertFixup();
        this.root.color = Color.Black;
    }

    @Override
    public void treeInsert(T data) {
        super.treeInsert(data);
    }

    @Override
    public Leaf<T> search(T data) {
        return super.search(data);
    }

    @Override
    public Leaf<T> minimum() {
        return super.minimum();
    }

    @Override
    public Leaf<T> maximum() {
        return super.maximum();
    }

    @Override
    public void delete(Leaf<T> leaf) {
        super.delete(leaf);
    }

    @Override
    public String toString() {
        return super.toString();
    }
}
