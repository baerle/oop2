package Vorlesungen.Mergesort;

public class MergeSortIterativ {
    public static void main(String[] args) {
        MergeSortIterativ test = new MergeSortIterativ();
        int[] test1 = {10, 9, 1, 7, 3, 2, 4, 6/*, 5*/};
        test.mergeSortSedgewick(test1);

        for (int value : test1) {
            System.out.println(value);
        }

        int[] test2 = {10, 9, 1, 7, 3, 2, 4, 6/*, 5*/};
        test.mergeSort(test2);

        for (int value : test2) {
            System.out.println(value);
        }
    }

    //PROBLEM: doesn't work for non potentials of 2
    public void mergeSort(int[] array) {
        for (int i = 2; i <= array.length; i *= 2) {
            for (int n = 0; n < array.length; n += i) {
                int maxMinimum = Math.min(n + i - 1, array.length - 1);
                int middle = Math.min((n + i / 2 - 1), maxMinimum);
                System.out.println("Merge " + n + " " + middle + " " + maxMinimum);
                merge(array, n, middle, maxMinimum);
            }
        }
    }

    public void mergeSortSedgewick(int[] array) {   //Bottom-up Mergesort
        for (int i = 1; i < array.length; i += i) {
            for (int n = 0; n < array.length - i; n += (i + i)) {
                int maxMinimum = Math.min(n + i + i - 1, array.length - 1);
                System.out.println("Merge " + n + " " + (n + i - 1) + " " + maxMinimum);
                merge(array, n, n + i - 1, maxMinimum);
            }
        }
    }

    private void merge(int[] array, int low, int mid, int high) {
        int n1 = mid - low + 1;
        int n2 = high - mid;
        int[] l = new int[n1 + 1];
        int[] re = new int[n2 + 1];
        for (int i = 0; i < n1; i++) {
            l[i] = array[low + i];
        }
        for (int i = 0; i < n2; i++) {
            re[i] = array[mid + i + 1];
        }
        l[n1] = Integer.MAX_VALUE;
        re[n2] = Integer.MAX_VALUE;

        int i = 0;
        int j = 0;
        for (int k = low; k <= high; k++) {
            if (l[i] <= re[j]) {
                array[k] = l[i++];
            } else {
                array[k] = re[j++];
            }
        }
    }
}
