package Vorlesungen.Mergesort;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Mergesort {
    private final int[] dataArray;
    private final int[] exchangeArray;

    public Mergesort(int[] array) {
        this.dataArray = array;
        this.exchangeArray = new int[this.dataArray.length + 2];

        this.mergeSort(0, this.dataArray.length - 1);
    }

    public Mergesort(List<Integer> list) {
        this.dataArray = new int[list.size()];
        this.exchangeArray = new int[this.dataArray.length + 2];
        int count = 0;
        for (Integer i : list) {
            this.dataArray[count++] = i;
        }

        this.mergeSort(0, this.dataArray.length - 1);
    }

    public void mergeSort(int from, int to) {
        if (from < to) {
            int middle = (to + from) / 2;
            this.mergeSort(from, middle);
            this.mergeSort(middle + 1, to);
            this.merge(from, middle, to);
        }
    }

    public void merge(int from, int middle, int until) {
        // Queue declaration
        int leftLength = middle - from + 1;
        int rightLength = until - middle;
        this.exchangeArray[leftLength] = Integer.MAX_VALUE;
        this.exchangeArray[leftLength + 1 + rightLength] = Integer.MAX_VALUE;

        // Queue initialisation
        for (int i = 0; i < leftLength; i++) {
            this.exchangeArray[i] = this.dataArray[from + i];
        }
        for (int i = 0; i < rightLength; i++) {
            this.exchangeArray[i + leftLength + 1] = this.dataArray[middle + i + 1];
        }

        int left = 0, right = leftLength + 1;
        for (int i = from; i <= until; i++) {
            if (this.exchangeArray[left] <= this.exchangeArray[right]) {
                this.dataArray[i] = this.exchangeArray[left++];
            } else {
                this.dataArray[i] = this.exchangeArray[right++];
            }
        }
    }

    public int[] getArray() {
        return dataArray;
    }

    public static void main(String[] args) {
        ArrayList<Integer> randoms = new ArrayList<>();
        try (BufferedReader reader = new BufferedReader(new FileReader("src/SubArraySum/randoms.txt"))) {
            String line;
            while ((line = reader.readLine()) != null) {
                String[] number = line.split(",");
                randoms.add(Integer.parseInt(number[0]));
                randoms.add(Integer.parseInt(number[1]));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

//        int count = randoms.size();
        int count = 10;
        int[] numbers = new int[count];
        for (int i = 0; i < count; i++) {
            numbers[i] = randoms.get(i);
        }
        Mergesort qs = new Mergesort(numbers);
        System.out.println(Arrays.toString(qs.getArray()));

        // Überprüfung der korrekten Sortierung
        boolean correct = true;
        int last = Integer.MIN_VALUE;
        for (int n : qs.getArray()) {
            if (last > n) {
                correct = false;
            }
            last = n;
        }
        System.out.println(correct);
    }
}
