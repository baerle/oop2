package Vorlesungen.Hello_World;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class Hello_World extends Application {

    @Override
    public void start(Stage stage) {
        Label question = new Label("Your Name:");
        Label message = new Label();

        TextField textField = new TextField();

        Button hello = new Button("Say Hello");
        Button exit = new Button("Exit");

        HBox buttons = new HBox();
        buttons.setSpacing(7.5);
        buttons.getChildren().addAll(hello, exit);

        VBox root = new VBox();
        root.setSpacing(7.5);
        root.getChildren().addAll(question, textField, message, buttons);

        Scene scene = new Scene(root, 350, 150);
        stage.setTitle("Hello World!");
        stage.setScene(scene);
        stage.show();

        hello.setOnAction(actionEvent -> {
            if (textField.getText().length() > 0) {
                message.setText("Hello " + textField.getText());
            } else {
                message.setText("Hello there");
            }
        });

        exit.setOnAction(e -> Platform.exit());
    }
}
