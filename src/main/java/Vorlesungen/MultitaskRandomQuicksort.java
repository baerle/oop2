package Vorlesungen;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;

public class MultitaskRandomQuicksort {
    private final int[] array;
    private ExecutorService es;
    private Semaphore sp;

    public MultitaskRandomQuicksort(int[] array, ExecutorService es) {
        this.array = array;
        this.es = es;
        this.sp = new Semaphore(8, false);
        try {
            this.sp.acquire();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        this.quicksort(0, array.length - 1);
        this.sp.release();
    }

    /**
     * @param from int index of first element
     * @param to   int index of last element
     */
    private void quicksort(int from, int to) {
        if (from < to) {
            try {
                this.sp.acquire();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            int pivot = partition(from, to);
//            System.out.println(pivot);
            this.es.execute(() -> this.quicksort(from, pivot - 1));
            this.es.execute(() -> this.quicksort(pivot + 1, to));

            this.sp.release();
        }
    }

    private int partition(int from, int to) {
        this.exchange(random(from, to), to);
        int pivot = this.array[to];
        int i = from - 1;
        for (int j = from; j < to; j++) {
            if (this.array[j] <= pivot) {
                this.exchange(++i, j);
            }
        }
        this.exchange(++i, to);
        return i;
    }

    private int random(int from, int to) {
        return (int) (Math.random() * to + from) % (to - from) + from;
    }

    /**
     * exchange element at position1 with position2
     *
     * @param pos  int position 1
     * @param pos2 int position 2
     */
    private void exchange(int pos, int pos2) {
        int temp = this.array[pos];
        this.array[pos] = this.array[pos2];
        this.array[pos2] = temp;
    }

    public int[] getArray() {
        try {
            this.sp.acquire(8);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        this.sp.release(8);
        return this.array;
    }

    public List<Integer> getList() {
        ArrayList<Integer> al = new ArrayList<>(this.array.length);
        for (int i : this.array) {
            al.add(i);
        }
        return al;
    }

    public static void main(String[] args) {
        ArrayList<Integer> randoms = new ArrayList<>();
        try (BufferedReader reader = new BufferedReader(new FileReader("src/Vorlesungen/randoms.txt"))) {
            String line;
            while ((line = reader.readLine()) != null) {
                String[] number = line.split(",");
                randoms.add(Integer.parseInt(number[0]));
                randoms.add(Integer.parseInt(number[1]));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        ExecutorService es = Executors.newFixedThreadPool(10);

        int count = randoms.size();
//        int count = 10;
        int[] numbers = new int[count];
        for (int i = 0; i < count; i++) {
            numbers[i] = randoms.get(i);
        }
        MultitaskRandomQuicksort qs = new MultitaskRandomQuicksort(numbers, es);
        try {
            Thread.sleep(100);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println(Arrays.toString(qs.getArray()));

        es.shutdown();
//        System.out.println("Waiting for termination");
        try {
            es.awaitTermination(60, TimeUnit.SECONDS);
        } catch (InterruptedException e) {
            e.printStackTrace();
            System.out.println("didn't wait 60 seconds");
        }

        // Überprüfung der korrekten Sortierung
        boolean correct = true;
        int last = Integer.MIN_VALUE;
        for (int n : qs.getArray()) {
            if (last > n) {
                correct = false;
            }
            last = n;
        }
        System.out.println("correct order: " + correct);
    }
}
