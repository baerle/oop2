package Vorlesungen;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.StackPane;
import javafx.scene.shape.Rectangle;
import javafx.stage.Stage;

public class EventFilterHandler extends Application {
    @Override
    public void start(Stage stage) {
        Rectangle rct = new Rectangle(20, 20);
        StackPane sp = new StackPane(rct);

        sp.addEventFilter(MouseEvent.MOUSE_RELEASED, e -> {
            System.out.println("Filter Stack Pane");
//            e.consume();
        });
        rct.addEventFilter(MouseEvent.MOUSE_RELEASED, e -> {
            System.out.println("Filter #1");
//            e.consume();
        });
        rct.addEventFilter(MouseEvent.MOUSE_RELEASED, e -> {
            System.out.println("Filter #2");
//            e.consume();
        });
        rct.addEventHandler(MouseEvent.MOUSE_RELEASED, e -> {
            System.out.println("Handler #1");
//            e.consume();
        });
        rct.addEventHandler(MouseEvent.MOUSE_RELEASED, e -> {
            System.out.println("Handler #2");
//            e.consume();
        });
        rct.setOnMouseReleased(e -> {
            System.out.println("convenience");
//            e.consume();
        });
        sp.addEventHandler(MouseEvent.MOUSE_RELEASED, e -> System.out.println("Stack Pane"));

        stage.setScene(new Scene(sp));
        stage.show();
    }
}
