package Vorlesungen.FurnishedHouse;

public class FurnishedHouse {
    String s = "House";

    public static void main(String[] args) {
        new FurnishedHouse().new Room().new Chair().output();
    }

    class Room {
        String s = "Room";

        class Chair {
            String s = "Chair";

            void output() {
                System.out.println(s);
                System.out.println(this.s);
                System.out.println(Room.this.s);
                System.out.println(FurnishedHouse.this.s);
            }
        }
    }
}
