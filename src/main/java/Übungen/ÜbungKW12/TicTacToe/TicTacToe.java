package Übungen.ÜbungKW12.TicTacToe;

import java.util.Scanner;

public class TicTacToe {
    private char[][] gamefield;
    private String playerx;
    private String playero;
    private boolean beginner;
    private int row;
    private int column;


    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Spielername Spieler x:");
        String playerx = scanner.nextLine();

        System.out.println("Spielername Spieler o:");
        String playero = scanner.nextLine();

        TicTacToe ticTacToe = new TicTacToe(playerx, playero);
        ticTacToe.run();
    }


    public TicTacToe(String playerx, String playero) {
        this.gamefield = new char[3][3];
        this.beginner = false;
        this.playerx = playerx;
        this.playero = playero;
    }

    public void run() {
        while(!hasWonGame()) {
            if (beginner) {
                System.out.println();
                System.out.println("It's your turn, " + playero);
                turn('o');
            } else {
                System.out.println();
                System.out.println("It's your turn, " + playerx);
                turn('x');
            }
        }
        System.out.println("Do you want to play again? y/n");

        Scanner scanner = new Scanner(System.in);
        String result = scanner.nextLine();
        switch (result.trim()) {
            case "y", "Y" -> {
                this.gamefield = new char[3][3];
                run();
            }
        }
    }

    private boolean hasWonGame() {
        char winner;
        if ((winner = hasWonHorizontal()) != Character.UNASSIGNED) {
            printGamefield();
            printWinner(winner, "horizontal");
            return true;
        }
        else if ((winner = hasWonVertical()) != Character.UNASSIGNED) {
            printGamefield();
            printWinner(winner, "vertical");
            return true;
        }
        else if ((winner = hasWonDiagonal()) != Character.UNASSIGNED) {
            printGamefield();
            printWinner(winner, "diagonal");
            return true;
        }
        else if ((winner = hasWonBackDiagonal()) != Character.UNASSIGNED) {
            printGamefield();
            printWinner(winner, "backdiagonal");
            return true;
        }
        else return isFieldFull();
    }

    private char hasWonHorizontal() {
        boolean hasWon = false;
        for (int i = 0; i < this.gamefield.length; i++) {
            char firstEntry = this.gamefield[i][0];
            if (firstEntry != Character.UNASSIGNED) {
                for (int j = 1; j < this.gamefield[i].length; j++) {
                    if (firstEntry != this.gamefield[i][j]) {
                        firstEntry = Character.UNASSIGNED;
                    }
                }
            }
            if (firstEntry != Character.UNASSIGNED) {
                return firstEntry;
            }
        }
        return Character.UNASSIGNED;
    }

    private char hasWonVertical() {
        boolean hasWon = false;
        for (int i = 0; i < this.gamefield.length; i++) {
            char firstEntry = this.gamefield[i][0];
            if (firstEntry != Character.UNASSIGNED) {
                for (int j = 1; j < this.gamefield[i].length; j++) {
                    if (firstEntry != this.gamefield[j][i]) {
                        firstEntry = Character.UNASSIGNED;
                    }
                }
            }
            if (firstEntry != Character.UNASSIGNED) {
                return firstEntry;
            }
        }
        return Character.UNASSIGNED;
    }

    private char hasWonDiagonal() {
        boolean hasWon = false;
        char firstEntry = this.gamefield[0][0];
        for (int i = 1; i < this.gamefield.length; i++) {
            if (firstEntry != Character.UNASSIGNED) {
                if (this.gamefield[i][i] != firstEntry) {
                    firstEntry = Character.UNASSIGNED;
                }
            }
        }
        if (firstEntry != Character.UNASSIGNED) {
            return firstEntry;
        }
        return Character.UNASSIGNED;
    }

    private char hasWonBackDiagonal() {
        boolean hasWon = false;
        char firstEntry = this.gamefield[0][this.gamefield.length-1];
        for (int i = 2; i <= this.gamefield.length; i++) {
            if (firstEntry != Character.UNASSIGNED) {
                if (this.gamefield[i-1][this.gamefield.length-i] != firstEntry) {
                    firstEntry = Character.UNASSIGNED;
                }
            }
        }
        if (firstEntry != Character.UNASSIGNED) {
            return firstEntry;
        }
        return Character.UNASSIGNED;
    }

    private void printWinner(char winner, String type) {
        System.out.println();
        if (winner == 'x') {
            System.out.println(this.playerx + " has won " + type + "!");
        } else {
            System.out.println(this.playero + " has won " + type + "!");
        }
    }

    private boolean isFieldFull() {
        for (int i = 0; i < this.gamefield.length; i++) {
            for (int j = 0; j < this.gamefield[i].length; j++) {
                if (this.gamefield[i][j] == Character.UNASSIGNED) {
                    return false;
                }
            }
        }
        System.out.println("You've got a draw!");
        return true;
    }

    private void turn(char player) {
        printGamefield();
        getRowAndCol();
        setField(player);
        beginner = !beginner;
    }

    private void printGamefield() {
        for (int i = 0; i < this.gamefield.length; i++) {
            System.out.print("|");
            for (int j = 0; j < this.gamefield[i].length; j++) {
                if (this.gamefield[i][j] == Character.UNASSIGNED) {
                    System.out.print("_|");
                } else {
                    System.out.print(this.gamefield[i][j] + "|");
                }
            }
            System.out.println();
        }
    }

    private void getRowAndCol() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Where do you set?");
        System.out.println("Row");
        this.row = getIntFrom(scanner);
        System.out.println("Column:");
        this.column = getIntFrom(scanner);
    }

    private int getIntFrom(Scanner scanner) {
        String input = scanner.nextLine();
        int inputInt = 0;
        try {
            inputInt = Integer.parseInt(input);
        } catch (NumberFormatException $e) {
            System.out.println("Not a number");
            getIntFrom(scanner);
        }
        if (inputInt > 0 && inputInt <= gamefield.length) {
            return inputInt;
        } else {
            System.out.println("Not a number in range");
            return getIntFrom(scanner);
        }
    }

    private void setField(char player) {
        if (this.gamefield[this.row-1][this.column-1] == Character.UNASSIGNED) {
            this.gamefield[this.row - 1][this.column - 1] = player;
        } else {
            System.out.println("Already taken");
            getRowAndCol();
            setField(player);
        }
    }
}
