package Übungen.ÜbungKW12.Atomictime;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;

public class Atomictime {
    public static void main(String[] args) {
        try {
            URL url = new URL("https://www.uhrzeit.org/atomuhr.php");

            BufferedReader br = new BufferedReader(new InputStreamReader(url.openConnection().getInputStream()));
            String line;
            while ((line = br.readLine()) != null)
                if (line.contains("<div class=\"anzeige zeit\">")) {
                    System.out.println(line.substring(30, 38));
                }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
