package Übungen.ÜbungKW24;

import com.sun.javafx.fxml.PropertyNotFoundException;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.fxml.FXML;
import javafx.scene.control.TextField;
import javafx.util.converter.NumberStringConverter;

public class Book implements Literature {
    private LiteraturManagement management;
    private Citations citations;

    @FXML
    private TextField titleTF, autorTF, yearTF, publisherTF, placeTF;

    private StringProperty title, autor, publisher, place;
    private IntegerProperty year;

    public Book() {
        this.title = new SimpleStringProperty();
        this.autor = new SimpleStringProperty();
        this.publisher = new SimpleStringProperty();
        this.place = new SimpleStringProperty();
        this.year = new SimpleIntegerProperty();
    }

    public Book(StringProperty title, StringProperty autor, StringProperty publisher, StringProperty place, IntegerProperty year) {
        this.title = title;
        this.autor = autor;
        this.publisher = publisher;
        this.place = place;
        this.year = year;
    }

    @FXML
    public void initialize() {
        this.titleTF.textProperty().bindBidirectional(this.title);
        this.autorTF.textProperty().bindBidirectional(this.autor);
        this.publisherTF.textProperty().bindBidirectional(this.publisher);
        this.placeTF.textProperty().bindBidirectional(this.place);
        this.yearTF.textProperty().bindBidirectional(this.year, new NumberStringConverter());
    }

    public void setCitations(Citations citations) {
        this.citations = citations;
    }

    @FXML
    public void save() {
        if (this.citations != null) {
            this.citations.add(this);
        } else throw new PropertyNotFoundException("citations isn't set!");
        this.management.showOverview();
    }

    public void setManagement(LiteraturManagement management) {
        this.management = management;
    }

    @Override
    public String getAPACitation() {
        StringBuilder sb = new StringBuilder();
        sb.append(this.title.get());
        sb.append(" (");
        sb.append(this.year.get());
        sb.append("). ");
        sb.append(this.title.get());
        sb.append(". ");
        sb.append(this.place.get());
        sb.append(": ");
        sb.append(this.publisher.get());
        sb.append('.');
        return sb.toString();
    }

    @Override
    public String toString() {
        return "Book{" +
                "title=" + title.get() +
                ", autor=" + autor.get() +
                ", publisher=" + publisher.get() +
                ", place=" + place.get() +
                ", year=" + year.get() +
                '}';
    }
}
