package Übungen.ÜbungKW24;

import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.Parent;
import javafx.scene.control.ListView;

public class OverviewListView extends Parent {
    @FXML
    private ListView<Book> listView;
    private ObservableList<Book> books;
    private Citations citations;

    public OverviewListView(Citations citations) {
        this.citations = citations;
    }

    @FXML
    public void initialize() {
        this.listView.getItems().setAll(this.citations.getItems());
    }

    public void setCitations(Citations citations) {
        this.citations = citations;
    }
}
