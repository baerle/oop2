package Übungen.ÜbungKW24;

import javafx.application.Application;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

import java.io.IOException;

public class LiteraturManagement extends Application {
    @FXML
    private GridPane bookGrid;
    @FXML
    private Book bookGridController;

    private final Citations citations;

    private Stage primaryStage;
    @FXML
    private OverviewListView overview;

    public static void main(String[] args) {
        launch(args);
    }

    public LiteraturManagement() {
        this.citations = new Citations();
        this.citations.add(new Book(new SimpleStringProperty("t1"), new SimpleStringProperty("a1"), new SimpleStringProperty("pub1"), new SimpleStringProperty("place1"), new SimpleIntegerProperty(2020)));
    }

    @FXML
    public void initialize() {
        this.bookGridController.setCitations(this.citations);
        this.bookGridController.setManagement(this);
    }

    @Override
    public void start(Stage primaryStage) throws IOException {
        //this.overview = new OverviewListView(this.citations);
        Parent root = FXMLLoader.load(getClass().getResource("./main.fxml"));
        primaryStage.setScene(new Scene(root));
        primaryStage.show();

        this.primaryStage = primaryStage;
    }

    public void showOverview() {
        if (this.overview != null) {
            /*primaryStage.setScene(new Scene(this.overview));
            primaryStage.show();*/
            this.bookGrid.setVisible(false);
            this.overview.setVisible(true);
        } else {
            System.out.println("overview null!");
        }
    }
}
