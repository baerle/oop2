package Übungen.ÜbungKW24;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

public class Citations {

    private final ObservableList<Book> books = FXCollections.observableArrayList();

    public Citations() {
    }

    public void add(Book book) {
        this.books.add(book);
        System.out.println(book);
        System.out.println(this);
    }

    public ObservableList<Book> getItems() {
        return this.books;
    }

    @Override
    public String toString() {
        return "Citations{" +
                "books=" + books +
                '}';
    }
}
