package Übungen.ÜbungKW18;

import java.util.List;

public class Main {
    public static void main(String[] args) {
        String test = "Meine Oma fährt im Hühnerstall Motorrad";
        List<String> list = ListUtils.init(test.length(), index -> test.substring(0, index + 1));
        list.forEach(System.out::println);

        ListUtils.init(10, i -> (i + 1) * 2).forEach(e -> System.out.print(e + ", "));
        System.out.println();

        ListUtils.map(ListUtils.init(10, i -> i), i -> {
            System.out.println(i);
            return i;
        });

        String result = ListUtils.findFirst(list, (String e) -> e.contains("Oma"));
        System.out.println(result);
    }
}
