package Übungen.ÜbungKW18;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;

public class ListUtils {
    public static <T> List<T> init(int length, Function<Integer, T> function) {
        List<T> list = new ArrayList<>(length);
        for (int index = 0; index < length; index++) {
            list.add(function.apply(index));
        }

        return list;
    }

    public static <T> T findFirst(List<T> list, Function<T, Boolean> function) {
        for (T i : list) {
            if (function.apply(i)) return i;
        }
        return null;
    }

    public static <T> List<T> map(List<T> list, Function<T, T> function) {
        for (T i : list) {
            function.apply(i);
        }

        return list;
    }
}
