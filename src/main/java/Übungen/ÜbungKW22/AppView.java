package Übungen.ÜbungKW22;

import javafx.beans.property.SimpleLongProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

public class AppView extends HBox {
    ObservableList<Student> students;
    DetailView detailView;

    public AppView(DetailView detailView) {
        this.detailView = detailView;
        Button sort = new Button("Sort");
        Button add = new Button("+");
        VBox controls = new VBox(sort, add);
        this.students = FXCollections.observableArrayList();    //TODO: Callback einbinden
        ListView<Student> listView = new ListView<>(students);
        super.getChildren().addAll(controls, listView, detailView);

        addStudent(new Student(new SimpleStringProperty("Patrick Bär"), new SimpleLongProperty(123456)));
        addStudent(new Student(new SimpleStringProperty("Andere"), new SimpleLongProperty(1238456)));

        listView.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue != null) detailView.setStudent(newValue);
        });
//        listView.itemsProperty().bindBidirectional(new SimpleListProperty<>(this.students));

        add.setOnMouseClicked(e -> {
            addStudent(new Student(new SimpleStringProperty(), new SimpleLongProperty()));
        });

        sort.setOnMouseClicked(e -> sort());
    }

    public void addStudent(Student student) {
        this.students.add(student);
        this.detailView.setStudent(student);
    }

    public void sort() {
        FXCollections.sort(this.students);
    }
}
