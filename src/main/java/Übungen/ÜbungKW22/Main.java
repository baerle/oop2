package Übungen.ÜbungKW22;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class Main extends Application {
    @Override
    public void start(Stage stage) {
        DetailView detailView = new DetailView();

        AppView appView = new AppView(detailView);

        Scene scene = new Scene(appView);

        stage.setScene(scene);
        stage.show();
    }
}
