package Übungen.ÜbungKW22;

import javafx.beans.property.LongProperty;
import javafx.beans.property.StringProperty;

public class Student implements Comparable<Student> {
    private StringProperty name;
    private LongProperty matrikelnr;

    public Student(StringProperty name, LongProperty matrikelnr) {
        this.name = name;
        this.matrikelnr = matrikelnr;
    }

    public String getName() {
        return name.get();
    }

    public StringProperty nameProperty() {
        return name;
    }

    public long getMatrikelnr() {
        return matrikelnr.get();
    }

    public LongProperty matrikelnrProperty() {
        return matrikelnr;
    }

    @Override
    public int compareTo(Student o) {
        return (this.matrikelnr.get() > o.getMatrikelnr() ? 1 : -1);
    }

    @Override
    public String toString() {
        return "Name= " + name.get() + "\nMatrikelnr= " + matrikelnr.get();
    }
}
