package Übungen.ÜbungKW22;

import javafx.beans.property.LongProperty;
import javafx.beans.property.SimpleLongProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.scene.control.TextField;
import javafx.scene.layout.VBox;
import javafx.util.StringConverter;

public class DetailView extends VBox {
    TextField name, matrikelnr;
    LongProperty oldMatrikelNr;
    StringProperty oldName;

    public DetailView() {
        this.name = new TextField();
        this.name.setPromptText("Name");
        this.matrikelnr = new TextField();
        this.matrikelnr.setPromptText("MatrikelNr");
        this.oldMatrikelNr = new SimpleLongProperty();
        this.oldName = new SimpleStringProperty();
        super.getChildren().addAll(this.name, this.matrikelnr);
    }

    public StringProperty getNameProperty() {
        return name.textProperty();
    }

    public StringProperty getMatrikelnrProperty() {
        return matrikelnr.textProperty();
    }

    public void setStudent(Student student) {
        this.name.textProperty().unbindBidirectional(oldName);
        this.matrikelnr.textProperty().unbindBidirectional(oldMatrikelNr);

        this.name.textProperty().bindBidirectional(student.nameProperty());
        this.oldName = student.nameProperty();
        this.matrikelnr.textProperty().bindBidirectional(student.matrikelnrProperty(), new StringConverter<>() {
            @Override
            public String toString(Number number) {
                return number.toString();
            }

            @Override
            public Number fromString(String s) {
                return Long.parseLong(s);
            }
        });
        this.oldMatrikelNr = student.matrikelnrProperty();
    }
}
