package Übungen.ÜbungKW27;

import SubArraySum.Stopwatch;
import Vorlesungen.GenericQuicksort;
import Vorlesungen.Mergesort.Mergesort;
import Vorlesungen.Quicksort;
import Vorlesungen.RandomQuicksort;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

public class Sorter<T> {
    public static void main(String[] args) {
        int capacity = 30_000_000;    //beginn int comparison
        ArrayList<Integer> numbers = new ArrayList<>(capacity);

        //Initialisation
        for (int i = 0; i < capacity; i++) {
            numbers.add((int) (Math.random() * 100_000_000) - 50_000_000);
        }

        ArrayList<Integer> numbers2 = new ArrayList<>(numbers);
        ArrayList<Integer> numbers3 = new ArrayList<>(numbers);
        ArrayList<Integer> numbers4 = new ArrayList<>(numbers);
        ArrayList<Integer> numbers5 = new ArrayList<>(numbers);
        ArrayList<Integer> numbers6 = new ArrayList<>(numbers);
        //end int comparison

        /*int capacity = 15_000_000;   //begin String comparison
        ArrayList<String> numbers = new ArrayList<>(capacity);

        for (int i = 0; i < capacity; i++) {
            numbers.add(generateRandomString());
        }

        ArrayList<String> numbers2 = new ArrayList<>(numbers);
        ArrayList<String> numbers3 = new ArrayList<>(numbers);
                                    //end String comparison*/


        Stopwatch stopwatch = new Stopwatch();

        new Sorter().multitask(numbers2, 4); //23 s

        System.out.println(stopwatch);

        numbers2.sort(null);    //for merging

        System.out.println("Multitask: " + stopwatch);


        stopwatch = new Stopwatch();

        numbers.sort(null); //33 - 34 s

        System.out.println("Normal: " + stopwatch);


        stopwatch = new Stopwatch();

        Integer[] array = new Integer[numbers3.size()];
        int count = 0;
        for (int i : numbers3) {
            array[count++] = i;
        }
        System.out.println("Initialisation GenericQuicksort: " + stopwatch);

        stopwatch = new Stopwatch();

        Integer[] array2 = array.clone();

        System.out.println("Initialisation Array Parallel Sort: " + stopwatch);

        Arrays.parallelSort(array2, null);

        System.out.println("Array Parallel Sort: " + stopwatch);


        stopwatch = new Stopwatch();

        GenericQuicksort<Integer> gqs = new GenericQuicksort<>(array);

        System.out.println("GenericQuicksort: " + stopwatch);


        stopwatch = new Stopwatch();

        Quicksort qs = new Quicksort(numbers4);

        System.out.println("Quicksort: " + stopwatch);


        stopwatch = new Stopwatch();

        RandomQuicksort rqs = new RandomQuicksort(numbers5);

        System.out.println("Random Quicksort: " + stopwatch);


        stopwatch = new Stopwatch();

        Mergesort ms = new Mergesort(numbers3);

        System.out.println("Mergesort: " + stopwatch);
    }

    public static String generateRandomString() {
        int leftLimit = 48; // numeral '0'
        int rightLimit = 122; // letter 'z'
        int targetStringLength = 10;
        Random random = new Random();

        return random.ints(leftLimit, rightLimit + 1)
                .filter(i -> (i <= 57 || i >= 65) && (i <= 90 || i >= 97))
                .limit(targetStringLength)
                .collect(StringBuilder::new, StringBuilder::appendCodePoint, StringBuilder::append)
                .toString();
    }

    private void multitask(ArrayList<T> numbers, int numThreads) {
        Thread[] threads = new Thread[numThreads];
        int until = 0, count = 0;
        int quarter = numbers.size() / numThreads;
        for (int i = 0; i < threads.length; i++) {
//            System.out.println(until);
            List<T> list = numbers.subList(until, until += quarter);
            Thread thread = new Thread(() -> list.sort(null));
            thread.start();
            threads[count++] = thread;
        }

        for (int i = 0; i < threads.length; i++) {
            try {
//                System.out.println(threads[i].getState());
                threads[i].join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
