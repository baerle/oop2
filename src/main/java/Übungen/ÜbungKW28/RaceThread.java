package Übungen.ÜbungKW28;

public class RaceThread extends Thread {
    private final RaceCondition rc;

    public RaceThread(RaceCondition rc) {
        this.rc = rc;
    }

    @Override
    public void run() {
        for (int i = 0; i < 1_000_000; i++) {
            this.rc.increase();
        }
    }
}
