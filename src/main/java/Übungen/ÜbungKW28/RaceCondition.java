package Übungen.ÜbungKW28;

public class RaceCondition {
    private int counter;

    public RaceCondition() {
        counter = 0;
    }

    public synchronized void increase() {
        counter++;
    }

    public int getCounter() {
        return this.counter;
    }

    public static void main(String[] args) throws InterruptedException {
        RaceCondition rc = new RaceCondition();
        Thread th1 = new RaceThread(rc);
        Thread th2 = new RaceThread(rc);

        th1.start();
        th2.start();

        th1.join();
        th2.join();

        System.out.println(rc.getCounter());
    }
}
