package Übungen.ÜbungKW28;

import java.util.concurrent.atomic.AtomicInteger;

public class RaceCondition2 extends RaceCondition {
    private AtomicInteger counter;

    public RaceCondition2() {
        counter = new AtomicInteger(0);
    }

    public void increase() {
        counter.incrementAndGet();
    }

    public int getCounter() {
        return this.counter.get();
    }

    public static void main(String[] args) throws InterruptedException {
        RaceCondition2 rc = new RaceCondition2();
        Thread th1 = new RaceThread(rc);
        Thread th2 = new RaceThread(rc);

        th1.start();
        th2.start();

        th1.join();
        th2.join();

        System.out.println(rc.getCounter());
    }
}
