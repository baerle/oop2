package Übungen.ÜbungKW28;

import java.util.concurrent.locks.ReentrantLock;

public class RaceCondition3 extends RaceCondition {
    private ReentrantLock rl = new ReentrantLock();
    private int counter;

    public RaceCondition3() {
        counter = 0;
    }

    public void increase() {
        rl.lock();
        counter++;
        rl.unlock();
    }

    public int getCounter() {
        return this.counter;
    }

    public static void main(String[] args) throws InterruptedException {
        RaceCondition3 rc = new RaceCondition3();
        Thread th1 = new RaceThread(rc);
        Thread th2 = new RaceThread(rc);

        th1.start();
        th2.start();

        th1.join();
        th2.join();

        System.out.println(rc.getCounter());
    }
}
