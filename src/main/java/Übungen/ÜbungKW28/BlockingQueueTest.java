package Übungen.ÜbungKW28;

import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;

public class BlockingQueueTest {

    public static void main(String[] args) {
        Queue<Integer> q = new ConcurrentLinkedQueue<>();
        Producer p = new Producer(q);
        Consumer c1 = new Consumer(q);
        Consumer c2 = new Consumer(q);
        new Thread(p).start();
        new Thread(c1).start();
        new Thread(c2).start();
    }
}

class Producer implements Runnable {
    private final Queue<Integer> queue;

    Producer(Queue<Integer> q) {
        queue = q;
    }

    public void run() {
//        try {
        for (int i = 0; i < 20; i++) {
            synchronized (queue) {
                queue.offer(produce());
                System.out.println(queue);
            }
        }
        /*} catch (InterruptedException ex) {

        }*/
    }

    Integer produce() {
        return (int) (Math.random() * 100);
    }
}

class Consumer implements Runnable {
    private final Queue<Integer> queue;

    Consumer(Queue<Integer> q) {
        queue = q;

    }

    public void run() {
//        try {
        for (int i = 0; i < 20; i++) {
            consume(queue.poll());
        }
        /*} catch (InterruptedException ex) {

        }*/
    }

    void consume(Object x) {
        System.out.println(x + ", " + queue);
    }
}