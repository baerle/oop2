package Übungen.ÜbungKW21;

import javafx.application.Application;
import javafx.beans.binding.Bindings;
import javafx.beans.binding.DoubleBinding;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.StringProperty;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import javafx.util.converter.NumberStringConverter;

public class Midnightformular extends Application {
    private TextField a, b, c;
    private Label result1, result2;

    @Override
    public void start(Stage stage) {

        this.a = new TextField();
        this.b = new TextField();
        this.c = new TextField();

        this.a.setPromptText("a");
        this.b.setPromptText("b");
        this.c.setPromptText("c");

        HBox hbox = new HBox(2, this.a, this.b, this.c);

        this.result1 = new Label();
        this.result2 = new Label();

        VBox vbox = new VBox(hbox, this.result1, this.result2);

        Scene scene = new Scene(vbox);
        stage.setScene(scene);
        stage.show();

        StringProperty aStringProperty = this.a.textProperty();
        StringProperty bStringProperty = this.b.textProperty();
        StringProperty cStringProperty = this.c.textProperty();
        DoubleProperty a = new SimpleDoubleProperty();
        DoubleProperty b = new SimpleDoubleProperty();
        DoubleProperty c = new SimpleDoubleProperty();
        Bindings.bindBidirectional(aStringProperty, a, new NumberStringConverter());
        Bindings.bindBidirectional(bStringProperty, b, new NumberStringConverter());
        Bindings.bindBidirectional(cStringProperty, c, new NumberStringConverter());

        DoubleBinding discriminant = Bindings.createDoubleBinding(() -> b.get() * b.get() - 4 * a.get() * c.get(), a, b, c);

        DoubleBinding solution1 = Bindings.createDoubleBinding(() -> (-b.get() + Math.sqrt(discriminant.get())) / 2 * a.get(), a, b, c);
        DoubleBinding solution2 = Bindings.createDoubleBinding(() -> (-b.get() - Math.sqrt(discriminant.get())) / 2 * a.get(), a, b, c);

        result1.textProperty().bind(solution1.asString());
        result2.textProperty().bind(solution2.asString());
    }
}
