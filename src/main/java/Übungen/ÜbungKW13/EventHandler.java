package Übungen.ÜbungKW13;

import javafx.application.Application;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.event.EventType;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;

import java.util.concurrent.atomic.AtomicBoolean;

public class EventHandler extends Application {
    AnchorPane root;
    HBox hbox;
    TextField textField;
    TextArea textArea;
    Label state, event;

    @Override
    public void start(Stage stage) {
        this.state = new Label("Event catcher:");
        this.textField = new TextField();
        this.event = new Label();

        this.hbox = new HBox(this.state, this.textField, this.event);

        this.textArea = new TextArea();
        this.textArea.setEditable(false);
        this.textArea.setWrapText(true);
        this.textArea.appendText("Events: ");

        this.root = new AnchorPane(this.hbox, this.textArea);
        AnchorPane.setTopAnchor(this.hbox, 1.0);
        AnchorPane.setBottomAnchor(this.textArea, 1.0);

        Scene scene = new Scene(this.root, 600, 300);
        stage.setScene(scene);
        stage.show();

        AtomicBoolean set = new AtomicBoolean(false);
        IntegerProperty count = new SimpleIntegerProperty(0);

        this.textField.addEventHandler(EventType.ROOT, e -> {
            if (e.getEventType().equals(MouseEvent.MOUSE_MOVED)) {
                if (!set.get()) {
                    this.textArea.appendText(e.getEventType().toString() + ", ");
                    set.set(true);
                }
                count.set(count.get() + 1);
            } else {
                this.textArea.appendText(e.getEventType().toString() + ", ");
                set.set(false);
                count.set(0);
            }
        });

        this.state.textProperty().bind(count.asString());
    }
}
