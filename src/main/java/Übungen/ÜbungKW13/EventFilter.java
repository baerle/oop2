package Übungen.ÜbungKW13;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;

public class EventFilter extends Application {
    public static void handle(String catcher, String msg) {
        System.out.println(msg + "\t" + catcher);
    }

    @Override
    public void start(Stage primaryStage) {
        StackPane stack = new StackPane();
        Label la1 = new Label("Label1");

        stack.getChildren().addAll(la1);

        la1.addEventHandler(MouseEvent.MOUSE_CLICKED, event -> handle("handler", "I am Label 1"));
        stack.addEventHandler(MouseEvent.MOUSE_CLICKED, event -> handle("handler", "I am the Stack"));

        la1.addEventFilter(MouseEvent.MOUSE_CLICKED, event -> handle("filter", "I am Label 1"));
        stack.addEventFilter(MouseEvent.MOUSE_CLICKED, event -> handle("filter", "I am the Stack"));

        Scene scene = new Scene(stack, 80, 80);
        primaryStage.setScene(scene);
        primaryStage.show();
    }
}
