package Übungen.ÜbungKW20;

import javafx.beans.property.*;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

public class Student {

    private final SimpleLongProperty matrikelnr;
    private StringProperty name;
    private ListProperty<Float> grades;

    public Student(long matrikelnr, String name) {
        this.matrikelnr = new SimpleLongProperty(matrikelnr);
        this.name = new SimpleStringProperty(name);
        this.grades = new SimpleListProperty<>(FXCollections.observableArrayList());
    }

    public long getMatrikelnr() {
        return matrikelnr.get();
    }

    public SimpleLongProperty matrikelnrProperty() {
        return matrikelnr;
    }

    public void setMatrikelnr(long matrikelnr) {
        this.matrikelnr.set(matrikelnr);
    }

    public String getName() {
        return name.get();
    }

    public void setName(String name) {
        this.name.set(name);
    }

    public StringProperty nameProperty() {
        return name;
    }

    public ObservableList<Float> getGrades() {
        return grades.get();
    }

    public void setGrades(ObservableList<Float> grades) {
        this.grades.set(grades);
    }

    public ListProperty<Float> gradesProperty() {
        return grades;
    }

    public void addGrade(float grade) {
        this.grades.add(grade);
    }
}
