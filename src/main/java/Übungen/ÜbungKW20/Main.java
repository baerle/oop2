package Übungen.ÜbungKW20;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.Scene;
import javafx.scene.control.TextField;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import javafx.util.StringConverter;

public class Main extends Application {
    public Main() {
        super();
    }

    @Override
    public void start(Stage stage) {
        Student student = new Student(11111, "Dummy");

        TextField matrikelnr = new TextField("11111");
//        matrikelnr.setEditable(false);
        TextField name = new TextField("Dummy");
        TextField grades = new TextField();
//        grades.setEditable(false);

        VBox vbox = new VBox(5, matrikelnr, name, grades);
        stage.setScene(new Scene(vbox, 200, 100));
        stage.show();

        name.textProperty().bindBidirectional(student.nameProperty());

        matrikelnr.textProperty().bindBidirectional(student.matrikelnrProperty(), new StringConverter<>() {
            @Override
            public String toString(Number number) {
                return number.toString();
            }

            @Override
            public Number fromString(String s) {
                return Long.parseLong(s);
            }
        });

        student.nameProperty().addListener((observable, oldValue, newValue) -> {
            System.out.println(oldValue + " -> " + newValue);
            name.setText(newValue);
        });

        student.gradesProperty().addListener((observable, oldValue, newValue) -> {
            StringBuilder gradesText = new StringBuilder();
            for (float grade : newValue) {
                gradesText.append(grade).append(", ");
            }
            gradesText.setLength(gradesText.length() - 2);
            grades.setText(gradesText.toString());

            /* alternative to above
            String res = newValue.stream().map(String::valueOf).collect(Collectors.joining(", "));
            System.out.println("Stream: " + res);*/
        });

        name.textProperty().addListener((observable, oldValue, newValue) -> student.setName(newValue));

        grades.textProperty().addListener((observable, oldValue, newValue) -> {
            System.out.println(newValue);
            ObservableList<Float> gradesList = student.getGrades();
            ObservableList<Float> gradesListNew = FXCollections.observableArrayList();
            int i = 0, end;
            while (i < newValue.length()) {
                end = newValue.indexOf(", ", i);
                if (end != -1) {
                    gradesListNew.add(Float.parseFloat(newValue.substring(i, end)));
                    i = end + 2;
                } else {
                    float num;
                    if (newValue.length() - i > 0 && (num = Float.parseFloat(newValue.substring(i))) > 0) {
                        gradesListNew.add(num);
                    }
                    break;
                }
            }
            if (!gradesList.equals(gradesListNew))
                student.setGrades(gradesListNew);
            System.out.println(gradesListNew);
        });


        Thread t = new Thread(() -> {
            try {
                Thread.sleep(1000);
                Platform.runLater(() -> student.setName("Julius Caesar"));
                Thread.sleep(1000);
                Platform.runLater(() -> student.addGrade(1f));
                Thread.sleep(1000);
                Platform.runLater(() -> student.addGrade(5f));
                Platform.runLater(() -> student.addGrade(3f));
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });
        t.setDaemon(true);
        t.start();
    }
}
