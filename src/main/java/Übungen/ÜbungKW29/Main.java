package Übungen.ÜbungKW29;

import java.util.Collection;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

public class Main {
    List<Student> students;

    public static void main(String[] args) {
        Main main = new Main();
    }

    public Main() {
        this.students = Student.generateStudents();

        this.aufgabe1();
        aufgabe2();
        aufgabe3();
        aufgabe4();
        aufgabe5();
        aufgabe6();
    }

    public void aufgabe1() {
        List<String> namen = students.stream().map(Student::getName).collect(Collectors.toList());
        namen.forEach(System.out::println);
    }

    public void aufgabe2() {
        System.out.println("Durchschnittsalter: " + students.stream().mapToDouble(Student::getAge).average());
    }

    public void aufgabe3() {
        //System.out.println("Durchschnittsnote: " + students.stream().map(Student::getGrades).flatMap(Collection::parallelStream).mapToDouble(d -> d).average());
        System.out.println(students.stream().map(Student::getGrades).flatMap(Collection::stream).mapToDouble(d -> d).average());
    }

    public void aufgabe4() {
        System.out.println("Zwischen 24 und 30: " + students.stream().filter(s -> s.getAge() <= 30 && s.getAge() >= 24).collect(Collectors.toList()));
    }

    public void aufgabe5() {
        System.out.println(new Random().ints(100, 1, 7).boxed().collect(Collectors.toList()));
    }

    public void aufgabe6() {
        System.out.println("sortiert: " + this.students.stream().sorted((s, s1) -> (int) Math.ceil(s.getAverageGrade()-s1.getAverageGrade())).collect(Collectors.toList()));
    }
}
