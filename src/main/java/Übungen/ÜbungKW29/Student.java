package Übungen.ÜbungKW29;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

public class Student implements Comparable<Student> {
    private String name;
    private List<Integer> grades;
    private int age;

    public Student(String name, List<Integer> grades, int age) {
        this.name = name;
        this.grades = grades;
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public List<Integer> getGrades() {
        return grades;
    }

    public int getAge() {
        return age;
    }

    @Override
    public String toString() {
        return "Student{" +
                "name='" + name + '\'' +
                ", grades=" + grades +
                ", age=" + age +
                '}';
    }

    public static List<Student> generateStudents() {
        List<Student> students = new ArrayList<>(15);
        for (int i = 0; i < 15; i++) {
            List<Integer> grades = new Random().ints(10, 1, 7).boxed().collect(Collectors.toList());
            students.add(new Student("name" + i, grades, i * 2));
        }

        students.stream().forEach(System.out::println);
        students.stream().forEach(s -> System.out.println(s.getGrades().stream().mapToDouble(d -> d).average()));

        return students;
    }

    @Override
    public int compareTo(Student o) {
        return this.name.compareTo(o.getName());
    }

    public double getAverageGrade() {
        return this.grades.stream().mapToDouble(d -> d).average().getAsDouble();
    }
}
