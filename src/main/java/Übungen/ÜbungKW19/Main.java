package Übungen.ÜbungKW19;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

import java.io.FileReader;
import java.util.ArrayList;
import java.util.Map;
import java.util.Properties;

import static java.lang.System.getenv;

public class Main extends Application {
    @Override
    public void start(Stage stage) throws Exception {
        int index = 0, width = 50, height = 30;
        ArrayList<Label> labels = new ArrayList<>();
        ArrayList<String> keys = new ArrayList<>();
        ArrayList<String> values = new ArrayList<>();

        // read-in all properties from file
        Properties properties = new Properties();
        properties.load(new FileReader("src/Übungen/ÜbungKW19/settings.properties"));
        for (String key : properties.stringPropertyNames()) {
            keys.add(key);
            values.add(properties.getProperty(keys.get(index++)));
        }

        // read-in all environment variables
        index = 0;
        for (String key : keys) {
            try {
                String value = getenv(key);
                if (!value.equals(""))
                    values.set(index, value);
            } catch (NullPointerException ignored) {
            }
            index++;
        }

        // read-in all parameters
        Parameters p = this.getParameters();
        Map<String, String> namedParams = p.getNamed();
        for (String key : namedParams.keySet()) {
            int i = 0;
            for (String listKey : keys) {
                if (key.equals(listKey))
                    values.set(i, namedParams.get(key));
                if (listKey.equals("width"))
                    width = Integer.parseInt(values.get(i));
                if (listKey.equals("height"))
                    height = Integer.parseInt(values.get(i));
                i++;
            }
        }

        // create labels for all keys
        for (int i = 0; i < keys.size(); i++)
            labels.add(new Label(keys.get(i) + ": " + values.get(i)));

        // create scene
        VBox vbox = new VBox();
        for (Label label : labels)
            vbox.getChildren().add(label);
        Scene scene = new Scene(vbox, width, height);
        stage.setScene(scene);
        stage.show();
    }
}
