package Übungen;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.FlowPane;
import javafx.stage.Stage;

public class ProgressBarViaSlider extends Application {
    @Override
    public void start(Stage stage) {
        ProgressBar pb = new ProgressBar(0);
        Slider slider = new Slider(-0.1, 1, 0);
        slider.setShowTickLabels(true);
        slider.setShowTickMarks(true);
        slider.majorTickUnitProperty().set(0.6);

        slider.valueProperty().addListener((o, oldV, newV) -> pb.setProgress((Double) newV));

        Tooltip tooltip = new Tooltip("Control the progress bar with the slider");
        pb.setTooltip(tooltip);

        ToggleGroup tg = new ToggleGroup();
        RadioButton rb1 = new RadioButton("Radio");
        rb1.setToggleGroup(tg);
        rb1.getProperties().put("text", "Radio");
        RadioButton rb2 = new RadioButton("TV");
        rb2.setToggleGroup(tg);
        rb2.getProperties().put("text", "TV");
        RadioButton rb3 = new RadioButton("Stream");
        rb3.setToggleGroup(tg);
        rb3.getProperties().put("text", "Stream");

        tg.selectedToggleProperty().addListener((e, oldV, newV) -> {
            System.out.println(newV.getProperties().get("text"));
        });

        Scene scene = new Scene(new FlowPane(pb, slider, rb1, rb2, rb3));
        String url = ProgressBarViaSlider.class.getResource("test.css").toExternalForm();
        scene.getStylesheets().add(url);
        stage.setScene(scene);
        stage.show();
    }
}
