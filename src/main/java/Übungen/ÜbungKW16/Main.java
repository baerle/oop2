package Übungen.ÜbungKW16;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class Main extends Application {

    private VBox vbox;
    private GridPane gridPane;
    private Label labelUsername, labelPassword;
    private TextField username;
    private PasswordField password;
    private Button login, switching;
    private Scene root;

    @Override
    public void start(Stage stage) {
        this.labelUsername = new Label("Username:");
        this.labelPassword = new Label("Password:");
        this.username = new TextField();
        this.username.setPromptText("Username");
        this.password = new PasswordField();
        this.password.setPromptText("Password");
        this.gridPane = new GridPane();
        GridPane.setConstraints(labelUsername, 0, 0);
        GridPane.setConstraints(labelPassword, 0, 1);
        GridPane.setConstraints(username, 1, 0);
        GridPane.setConstraints(password, 1, 1);
        this.gridPane.getChildren().addAll(labelUsername, labelPassword, username, password);
        this.login = new Button("Login");
        this.login.getStyleClass().add("login");
        this.switching = new Button("Switch");
        this.switching.getStyleClass().add("switch");
        this.vbox = new VBox(this.gridPane, this.login, this.switching);
        this.root = new Scene(this.vbox, 300, 150);
        String styleURL = Main.class.getResource("styles.css").toExternalForm();
        this.root.getStylesheets().add(styleURL);

        stage.setScene(this.root);
        stage.show();
    }
}
